//
//  BasketBallViewController.m
//  LabBrain
//
//  Created by dlut on 14/11/6.
//  Copyright (c) 2014年 dlut. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()
@property int width;
@property NSMutableArray *listInforms;
@property NSMutableArray *listDates;
@property UILabel *labelTrend;
@property UILabel *labelInform;
@property UITextView *inform;
@property UITableView *tableTrends;
@property UIPageControl *pageControl;
@property int currentPageIndex;
@property NSMutableArray *images;
@property UICollectionView *collectionView;
@property int end;
@property int will;
@end

@implementation HomeViewController
@synthesize listInforms;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc]init];
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    self.width=CGRectGetWidth(self.view.frame);
    self.collectionView=[[UICollectionView alloc]initWithFrame:CGRectMake(0, 64, self.width, 200) collectionViewLayout:layout];
    [self.collectionView registerClass:[UICollectionViewCell class]forCellWithReuseIdentifier:@"cell"];
    [self.collectionView setBackgroundColor:[UIColor whiteColor]];
    self.collectionView.delegate=self;
    self.collectionView.dataSource=self;
    [self.collectionView setShowsHorizontalScrollIndicator:NO];
    [self.view addSubview:self.collectionView];
    self.listInforms=[[NSMutableArray alloc]init];
    [self.listInforms addObject:@"招聘会！"];
    [self.listInforms addObject:@"乒乓球比赛！"];
    [self.listInforms addObject:@"新生开学典礼！"];
    self.listDates=[[NSMutableArray alloc]init];
    [self.listDates addObject:@"11-10"];
    [self.listDates addObject:@"11-01"];
    [self.listDates addObject:@"10-16"];
    
    self.images=[[NSMutableArray alloc]init];
    [self.images addObject:@"nba.png"];
    [self.images addObject:@"computer.png"];
    [self.images addObject:@"travel.png"];
    
    self.currentPageIndex=0;
    self.end=0;
    self.will=0;
    self.pageControl = [[UIPageControl alloc]initWithFrame:CGRectMake(0, 244, self.width, 20)];
    [self.pageControl setCenter:CGPointMake(self.width/2, 254)];
    [self.pageControl setCurrentPage:self.currentPageIndex];
    [self.pageControl setNumberOfPages:3];
    [self.pageControl setCurrentPageIndicatorTintColor:[UIColor colorWithRed:46/255.0 green:192/255.0 blue:255/255.0 alpha:1]];
    [self.view addSubview:self.pageControl];
    self.labelInform=[[UILabel alloc]initWithFrame:CGRectMake(0,264,self.width, 25)];
    [self.labelInform setText:@"最新通知:"];
//    [self.labelInform setBackgroundColor:[UIColor colorWithRed:46/255.0 green:204/255.0 blue:255/255.0 alpha:1]];
    [self.view addSubview:self.labelInform];
    self.inform=[[UITextView alloc]initWithFrame:CGRectMake(0, 289, self.width, 75)];
    [self.inform setBackgroundColor:[UIColor colorWithRed:46/255.0 green:192/255.0 blue:255/255.0 alpha:1]];
    [self.inform setText:@"刘长春体育馆是巴拉巴拉巴拉巴拉巴啊哈哈哈哈哈哈哈哈哈哈哈哈哈。"];
    [self.inform setEditable:NO];
    [self.inform setShowsVerticalScrollIndicator:YES];
    [self.view addSubview:self.inform];
    self.labelTrend=[[UILabel alloc]initWithFrame:CGRectMake(0,364,self.width, 25)];
    [self.labelTrend setText:@"最新动态:"];
//    [self.labelTrend setBackgroundColor:[UIColor colorWithRed:46/255.0 green:204/255.0 blue:255/255.0 alpha:1]];
    [self.view addSubview:self.labelTrend];
    self.tableTrends=[[UITableView alloc]initWithFrame:CGRectMake(0, 389, self.width, 150) style:UITableViewStylePlain];
    [self.tableTrends setDelegate:self];
    [self.tableTrends setDataSource:self];
    [self.view addSubview:self.tableTrends];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 3;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    UIImageView *image=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.width, 200)];
    [image setImage:[UIImage imageNamed:[self.images objectAtIndex:[indexPath section]]]];
    [cell addSubview:image];
    [cell setBackgroundColor:[UIColor redColor]];
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.width, 200);
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.listInforms count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *title=@"inform";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:title];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:title];
    }
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    [cell.textLabel setText:[self.listInforms objectAtIndex:[indexPath row]]];
    [cell.detailTextLabel setText:[self.listDates objectAtIndex:[indexPath row]]];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    self.end=(int)[indexPath section];
    NSLog(@"end:%ld",(long)[indexPath section]);
    if (self.end==0 && self.will==1) {
        self.currentPageIndex=self.will;
        [self.pageControl setCurrentPage:self.will];
        [self.collectionView setContentOffset:CGPointMake(self.width*self.will, 0) animated:YES];
    }else if (self.end==1 && self.will==2){
        self.currentPageIndex=self.will;
        [self.pageControl setCurrentPage:self.will];
        [self.collectionView setContentOffset:CGPointMake(self.width*self.will, 0) animated:YES];
    }else if (self.end==1 && self.will==0){
        self.currentPageIndex=self.will;
        [self.pageControl setCurrentPage:self.will];
        [self.collectionView setContentOffset:CGPointMake(self.width*self.will, 0) animated:YES];
    }else if (self.end==2 && self.will==1){
        self.currentPageIndex=self.will;
        [self.pageControl setCurrentPage:self.will];
        [self.collectionView setContentOffset:CGPointMake(self.width*self.will, 0) animated:YES];
    }
}

-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    self.will=(int)[indexPath section];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
