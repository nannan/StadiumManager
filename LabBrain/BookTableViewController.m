//
//  BookTableViewController.m
//  LabBrain
//
//  Created by dlut on 14/10/31.
//  Copyright (c) 2014年 dlut. All rights reserved.
//

#import "BookTableViewController.h"
#import "CategoryData.h"

@interface BookTableViewController ()
@end

@implementation BookTableViewController
@synthesize list;
@synthesize refresh;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.list=[[NSMutableArray alloc]init];
    CategoryData *cat1=[[CategoryData alloc]initWithName:@"篮球" Image:@"basketball" Num:4];
    CategoryData *cat2=[[CategoryData alloc]initWithName:@"羽毛球" Image:@"badminton" Num:8];
    CategoryData *cat3=[[CategoryData alloc]initWithName:@"乒乓球" Image:@"tableTennis" Num:30];
    CategoryData *cat4=[[CategoryData alloc]initWithName:@"台球" Image:@"taiqiu" Num:15];
    CategoryData *cat5=[[CategoryData alloc]initWithName:@"游泳" Image:@"swimming" Num:60];
    [self.list addObject:cat1];
    [self.list addObject:cat2];
    [self.list addObject:cat3];
    [self.list addObject:cat4];
    [self.list addObject:cat5];
    [self setBeginRefreshing];
    [self setRefreshControl:self.refresh];
}

-(void)setBeginRefreshing{
    self.refresh=[[UIRefreshControl alloc]init];
    [self.refresh addTarget:self action:@selector(refreshAction:) forControlEvents:UIControlEventValueChanged];
}

-(void)refreshAction:(UIRefreshControl*)refresh{
    if (refresh.isRefreshing) {
        [self performSelector:@selector(refreshData) withObject:nil afterDelay:2];
    }
}

-(void)refreshData{
    [self.refresh endRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    // Return the number of rows in the section.
//    return [self.list count];
    return [self.list count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *title=@"book";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:title];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:title];
    }
    CategoryData *device=(CategoryData*)[self.list objectAtIndex:[indexPath row]];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    [cell.textLabel setText:[device name]];
    [cell.detailTextLabel setText:[NSString stringWithFormat:@"%d",[device num]]];
    [cell.imageView setImage:[UIImage imageNamed:[device image]]];
    // Configure the cell...
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
