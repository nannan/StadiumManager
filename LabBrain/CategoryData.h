//
//  CategoryData.h
//  LabBrain
//
//  Created by dlut on 14/11/14.
//  Copyright (c) 2014年 dlut. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryData : NSObject
@property NSString *name;
@property NSString *image;
@property int num;

-(id)initWithName:(NSString*)name Image:(NSString*)image Num:(int)num;
@end
