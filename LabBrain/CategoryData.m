//
//  CategoryData.m
//  LabBrain
//
//  Created by dlut on 14/11/14.
//  Copyright (c) 2014年 dlut. All rights reserved.
//

#import "CategoryData.h"

@implementation CategoryData
@synthesize name,image,num;

-(id)initWithName:(NSString *)name Image:(NSString *)image Num:(int)num{
    if (self=[super init]) {
        [self setName:name];
        [self setImage:image];
        [self setNum:num];
    }
    return self;
}
@end
