//
//  BookTableViewController.h
//  LabBrain
//
//  Created by dlut on 14/10/31.
//  Copyright (c) 2014年 dlut. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookTableViewController : UITableViewController
@property NSMutableArray *list;
@property UIRefreshControl *refresh;
@end
