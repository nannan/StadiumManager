//
//  BasketBallViewController.h
//  LabBrain
//
//  Created by dlut on 14/11/6.
//  Copyright (c) 2014年 dlut. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController:UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>

@end
