//
//  HttpTestViewController.h
//  LabBrain
//
//  Created by dlut on 14/11/14.
//  Copyright (c) 2014年 dlut. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HttpTestViewController : UIViewController<NSURLConnectionDataDelegate>

@end
