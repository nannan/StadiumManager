//
//  StadiumViewController.m
//  LabBrain
//
//  Created by dlut on 14/11/12.
//  Copyright (c) 2014年 dlut. All rights reserved.
//

#import "StadiumViewController.h"
@interface StadiumViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property UIImageView *leftImageView;
@property UIImageView *centerImageView;
@property UIImageView *rightImageView;
@property  UIPageControl *pageControl;
@property NSMutableArray *imageData;
@property int width;
@property int height,a;
//aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
@property int currentImageIndex;
@property int imageCount;
@property NSMutableArray *images;
@property (weak, nonatomic) IBOutlet UIScrollView *mScrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnToday;
@end

@implementation StadiumViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.width=CGRectGetWidth(self.view.frame);
    self.height=CGRectGetHeight(self.view.frame);
    self.images=[[NSMutableArray alloc]init];
    [self.images addObject:@"nba.png"];
    [self.images addObject:@"nba.png"];
    [self.images addObject:@"nba.png"];
    self.imageCount=3;
    self.currentImageIndex=0;
    [self addScrollView];
    [self addImageViews];
    [self addPageControl];
    [self setDefaultImage];
    [self.mScrollView setContentSize:CGSizeMake(self.width,600)];
    [self.btnToday.layer setCornerRadius:5];
}

-(void)addScrollView{
    //self.scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, self.width, 200)];
    [self.scrollView setDelegate:self];
    [self.scrollView setContentSize:CGSizeMake(self.width*self.imageCount, 200)];
    [self.scrollView setContentOffset:CGPointMake(self.width, 0) animated:YES];
    [self.scrollView setPagingEnabled:YES];
    [self.scrollView setShowsHorizontalScrollIndicator:YES];
    [self.scrollView setShowsVerticalScrollIndicator:NO];
//    [self.scrollView setBackgroundColor:[UIColor greenColor]];
//    [self.view addSubview:self.scrollView];
}
- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)addImageViews{
    self.leftImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.width, 200)];
    [self.leftImageView setContentMode:UIViewContentModeScaleAspectFill];
    [self.scrollView addSubview:self.leftImageView];
    self.centerImageView=[[UIImageView alloc]initWithFrame:CGRectMake(self.width, 0, self.width, 200)];
    [self.centerImageView setContentMode:UIViewContentModeScaleAspectFill];
    [self.scrollView addSubview:self.centerImageView];
    self.rightImageView=[[UIImageView alloc]initWithFrame:CGRectMake(self.width*2, 0, self.width, 200)];
    [self.rightImageView setContentMode:UIViewContentModeScaleAspectFill];
    [self.scrollView addSubview:self.rightImageView];
}

-(void)setDefaultImage{
    [self.leftImageView setImage:[UIImage imageNamed:[self.images objectAtIndex:0]]];
    [self.centerImageView setImage:[UIImage imageNamed:[self.images objectAtIndex:1]]];
    [self.rightImageView setImage:[UIImage imageNamed:[self.images objectAtIndex:2]]];
}

-(void)addPageControl{
    self.pageControl=[[UIPageControl alloc]init];
    CGSize size=[self.pageControl sizeForNumberOfPages:self.imageCount];
    [self.pageControl setBounds:CGRectMake(0, 0, size.width, 20)];
    [self.pageControl setCenter:CGPointMake(self.width/2, 260)];
    [self.pageControl setCurrentPage:self.currentImageIndex];
    [self.pageControl setCurrentPageIndicatorTintColor:[UIColor blueColor]];
    [self.pageControl setNumberOfPages:self.imageCount];
    [self.view addSubview:self.pageControl];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    CGPoint offset=[self.scrollView contentOffset];
    if (offset.x>self.width) {
        self.currentImageIndex=(self.currentImageIndex+1)%self.imageCount;
    }else if (offset.x<self.width){
        self.currentImageIndex=(self.currentImageIndex-1+self.imageCount)%self.imageCount;
    }
    [self.centerImageView setImage:[UIImage imageNamed:[self.images objectAtIndex:self.currentImageIndex]]];
    int left=(self.currentImageIndex-1+self.imageCount)%self.imageCount;
    int right=(self.currentImageIndex+1)%self.imageCount;
    [self.leftImageView setImage:[UIImage imageNamed:[self.images objectAtIndex:left]]];
    [self.rightImageView setImage:[UIImage imageNamed:[self.images objectAtIndex:right]]];
    [self.scrollView setContentOffset:CGPointMake(self.width, 0)];
    [self.pageControl setCurrentPage:self.currentImageIndex];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
